help:
	@echo To have a clean start, type "make clean"

clean:
	rm -f admin.conf kubeadm.join *.retry
	vagrant destroy
