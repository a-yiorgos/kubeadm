This is a small template for a [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm/) laboratory using vagrant. It is work in progress.  Because the VirtualBox provider does not support `--parallel`, the initial startup takes a lot of time to complete. You may need to `vagrant reload` the machines after the initial startup.

After a successful `vagrant up` you should be able to talk to the cluster with `KUBECONFIG=admin.conf kubectl get nodes`

## Add new worker
- add new worker definition in `machines.yml`
- delete `kubeadm.join`
- `vagrant up --provision`

---

TODO:
- better README
- I do not like it that Calico uses 192.168.0.0/16, so change this to
  something configurable?
- kubeadm init should advertise 10.0.1.10 (the master) in a configurable way.  I need some way to make machines.yml visible both to ansible and Vagrant


